//
//  NewsTarget.swift
//  NewsUsingViper
//
//  Created by Edwin Niwarlangga on 08/02/24.
//

import Moya

public enum NewsTarget {
    case getNewsSources
    case getArticles(page: Int, limit: Int, querySearch: String, source: String, category: String)
}

extension NewsTarget: TargetType {
    public var baseURL: URL {
        return URL(string: ConstantURL.urlString)!
    }
    
    public var path: String {
        switch self {
        case .getNewsSources:
            return "/top-headlines/sources"
        case .getArticles:
            return "/top-headlines"
        }
    }
    
    public var method: Moya.Method {
        switch self {
        case .getNewsSources, .getArticles:
            return .get
        }
    }
    
    public var sampleData: Data {
        return Data()
    }
    
    public var task: Task {
        switch self{
        case .getNewsSources:
            return .requestParameters(
                parameters: [
                    "apiKey": ConstantAuth.APIKEY
                ],
                encoding: URLEncoding.default)
        case let .getArticles(page, limit, querySearch, source, category):
            return .requestParameters(
                parameters: [
                    "source": source,
                    "category": category,
                    "page": page,
                    "pageSize": limit,
                    "q": querySearch,
                    "apiKey": ConstantAuth.APIKEY],
                encoding: URLEncoding.default)
        }
        
    }
    
    public var headers: [String: String]? {
        return ["Content-Type": "application/json"]
    }
    
    public var validationType: ValidationType {
        return .successCodes
    }
}
