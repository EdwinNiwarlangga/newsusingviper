//
//  Enum.swift
//  NewsUsingViper
//
//  Created by Edwin Niwarlangga on 08/02/24.
//

import Foundation

enum ErrorEnum: Error{
    case failed
    case errorDecode
}
