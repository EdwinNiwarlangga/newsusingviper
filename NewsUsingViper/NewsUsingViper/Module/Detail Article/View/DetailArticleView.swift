//
//  DetailArticleView.swift
//  NewsUsingViper
//
//  Created by Edwin Niwarlangga on 08/02/24.
//
import Foundation
import UIKit
import WebKit

class DetailArticleView: UIViewController, DetailArticleViewProtocol{
    
    var presenter: DetailArticlePresenterProtocol?
    var urlLink : String = ""
    var titleArticle: String = ""
    
    
    // MARK: UI Component
    @IBOutlet weak var navigationView: UIView!
    
    @IBOutlet weak var webView: WKWebView!
    @IBOutlet weak var articleWebTitleLabel: UILabel!
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        articleWebTitleLabel.text = self.titleArticle
        setWkWebView()
    }
    override func viewDidLoad() {
        LoadingScreen.sharedInstance.showIndicator()
        super.viewDidLoad()
    }
    
    func setWkWebView(){
        webView.navigationDelegate = self
        
        let urlDefault = URL(string: urlLink) ?? ConstantURL.webViewDefaultValue()
        
        webView.load(URLRequest(url: urlDefault))
        webView.allowsBackForwardNavigationGestures = true
    }
    
    func setUINavigation(){
        navigationView.backgroundColor = UIColor.white
        navigationView.layer.shadowColor = UIColor.gray.cgColor
        navigationView.layer.shadowOffset = CGSize(width: 1, height: 1)
        navigationView.layer.shadowRadius = 1
        navigationView.layer.shadowOpacity = 5
    }
    
}


extension DetailArticleView : WKNavigationDelegate{
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 3.0) {
            LoadingScreen.sharedInstance.hideIndicator()
        }
    }
}
