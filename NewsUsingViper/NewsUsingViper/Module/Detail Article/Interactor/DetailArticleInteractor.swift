//
//  DetailArticleInteractor.swift
//  NewsUsingViper
//
//  Created by Edwin Niwarlangga on 08/02/24.
//

import Foundation

class DetailArticleInteractor: DetailArticleInputInteractorProtocol{
    var presenter: DetailArticleOutputInteractorProtocol?
}
