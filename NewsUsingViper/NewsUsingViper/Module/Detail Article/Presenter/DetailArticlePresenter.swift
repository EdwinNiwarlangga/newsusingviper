//
//  DetailArticlePresenter.swift
//  NewsUsingViper
//
//  Created by Edwin Niwarlangga on 08/02/24.
//

import Foundation

class DetailArticlePresenter: DetailArticlePresenterProtocol{
    var urlLink: String?
    var titleArticle: String?
    
    var view: DetailArticleViewProtocol?
    var interactor: DetailArticleInputInteractorProtocol?
    var router: DetailArticleRouterProtocol?
    
    
}

extension DetailArticlePresenter: DetailArticleOutputInteractorProtocol{
    
}
