//
//  DetailArticleRouter.swift
//  NewsUsingViper
//
//  Created by Edwin Niwarlangga on 08/02/24.
//

import Foundation

class DetailArticleRouter: DetailArticleRouterProtocol{
    static func createDetailArticleModule(detailArticleRef detailArticleRef: DetailArticleView, urlSourceLink: String, title: String) {
        let presenter: DetailArticlePresenterProtocol & DetailArticleOutputInteractorProtocol = DetailArticlePresenter()
         
        detailArticleRef.urlLink = urlSourceLink
        detailArticleRef.titleArticle = title
        
        detailArticleRef.presenter = presenter
        detailArticleRef.presenter?.router = DetailArticleRouter()
        detailArticleRef.presenter?.view = detailArticleRef
        detailArticleRef.presenter?.interactor = DetailArticleInteractor()
        detailArticleRef.presenter?.interactor?.presenter = presenter
    }
}
