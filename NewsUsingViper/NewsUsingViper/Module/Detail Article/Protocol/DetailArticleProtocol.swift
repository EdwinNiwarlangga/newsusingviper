//
//  DetailArticleProtocol.swift
//  NewsUsingViper
//
//  Created by Edwin Niwarlangga on 08/02/24.
//
import Foundation
import UIKit

protocol DetailArticlePresenterProtocol: class{
    //View -> Presenter
    var view: DetailArticleViewProtocol? { get set }
    var interactor: DetailArticleInputInteractorProtocol? { get set }
    var router: DetailArticleRouterProtocol? { get set }
}

protocol DetailArticleViewProtocol: class{
    // PRESENTER -> VIEW
}

protocol DetailArticleInputInteractorProtocol: class{
    //Presenter -> Interactor
    var presenter : DetailArticleOutputInteractorProtocol? { get set }
}

protocol DetailArticleOutputInteractorProtocol: class {
    //Interactor -> Presenter
}

protocol DetailArticleRouterProtocol: class{
    //Presenter -> Wireframe
    static func createDetailArticleModule(detailArticleRef: DetailArticleView, urlSourceLink: String, title: String)
}

