//
//  SourceTableViewCell.swift
//  NewsUsingViper
//
//  Created by Edwin Niwarlangga on 08/02/24.
//

import UIKit

class SourceTableViewCell: UITableViewCell {
    @IBOutlet weak var viewRounded: UIView!
    @IBOutlet weak var sourceLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        resetUI()
    
        viewRounded.backgroundColor = UIColor.white
        viewRounded.layer.cornerRadius = 15.0
        viewRounded.layer.shadowColor = UIColor.gray.cgColor
        viewRounded.layer.shadowOffset = CGSize(width: 1, height: 1)
        viewRounded.layer.shadowRadius = 1
        viewRounded.layer.shadowOpacity = 5
    }
    
    func resetUI(){
        self.sourceLabel.text = ""
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setCell(name: String, country: String, description: String){
        DispatchQueue.main.async {
            self.sourceLabel.text = "\(name) - (\(country))"
            self.descriptionLabel.text = description
        }
    }
    
}

