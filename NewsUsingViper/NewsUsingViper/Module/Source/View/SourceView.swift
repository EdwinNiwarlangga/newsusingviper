//
//  SourceView.swift
//  NewsUsingViper
//
//  Created by Edwin Niwarlangga on 08/02/24.
//
import Foundation
import UIKit

class SourceView: UIViewController, SourcesViewProtocol{
    var presenter: SourcesPresenterProtocol?
    var categoryNews: String?
    var sourcesList = [SourceEntity]()
    var isLoading = false
    var isCanLoadMore = true
    
    @IBOutlet weak var sourcesTextField: UITextField!
    @IBOutlet weak var sourceTableView: UITableView!
    
    @IBOutlet weak var errorView: UIView!
    @IBOutlet weak var errorLabel: UILabel!
    @IBOutlet weak var errorImageView: UIImageView!
    
    override func viewDidLoad() {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            LoadingScreen.sharedInstance.showIndicator()
            self.isLoading = true
        }
        errorImageView.image = UIImage(named: "errorImage")
        errorImageView.isHidden = true
        errorLabel.isHidden = true
        errorView.isHidden = true
        
        presenter?.categoryNews = categoryNews
        presenter?.viewDidLoad(querySearch: sourcesTextField.text ?? "")
        
        
        sourceTableView.register(UINib.init(nibName: "SourceTableViewCell", bundle: .main), forCellReuseIdentifier: "sourceCell")
        sourceTableView.delegate = self
        sourceTableView.dataSource = self
        
        sourcesTextField.delegate = self
    }
    
    func updateSources(with sources: [SourceEntity], isSearch: Bool) {
        if isSearch {
            sourcesList = []
        }
        
        if sources.count > 0 {
            sources.forEach{ source in
                sourcesList.append(source)
            }
            
            isCanLoadMore = sources.count == 10 ? true : false
            
        } else{
            sourcesList = sources
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) { [weak self] in
            guard let self = self else { return }
            LoadingScreen.sharedInstance.hideIndicator()
            self.isLoading = false
            self.sourceTableView.reloadData()
            self.sourceTableView.isHidden = false
        }
    }
    
    func updateWithError(with error: String) {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            self.errorLabel.text = error
            LoadingScreen.sharedInstance.hideIndicator()
            self.sourceTableView.isHidden = true
            self.errorLabel.isHidden = false
            self.errorImageView.isHidden = false
            self.errorView.isHidden = false
        }
    }
    
}



extension SourceView: UITableViewDelegate{}

extension SourceView: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sourcesList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "sourceCell", for: indexPath) as? SourceTableViewCell
        let row = indexPath.row
        let source = sourcesList[row]
        
        cell?.setCell(name: source.name ?? "", country: source.country ?? "", description: source.description ?? "no description")
        return cell ?? UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let row = indexPath.row
        let source = sourcesList[row]
        
        presenter?.goToNewsListWithCategoryAndSource(with: self.categoryNews ?? "all", source: source.id ?? "", from: self)
    }
    
    // MARK: NEWS API DIDNT HAVE LIMIT ON SOURCES
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let getCountSources = sourcesList.count

        if indexPath.row == getCountSources - 2 && !isLoading && isCanLoadMore{
            isLoading = true
            LoadingScreen.sharedInstance.showIndicator()
            guard let searchText = sourcesTextField.text else {return}
            presenter?.loadMoreSources(isSearch: searchText == "" ? false : true, querySearch: searchText)
        }

    }
    
    
}

extension SourceView : UITextFieldDelegate{
    func textFieldDidChangeSelection(_ textField: UITextField) {
        guard let searchText = textField.text else { return }
        presenter?.searchSources(querySearch: searchText)
    }
    
    func hideKeyboardWhenTappedAround() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(SourceView.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}
