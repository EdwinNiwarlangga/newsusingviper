//
//  SourceInteractor.swift
//  NewsUsingViper
//
//  Created by Edwin Niwarlangga on 08/02/24.
//

import Foundation
import Moya

class SourceInteractor: SourcesInputInteractorProtocol{
    var presenter: SourcesOutputInteractorProtocol?
    let provider = MoyaProvider<NewsTarget>()
    
    func getSources() {
        provider.request(.getNewsSources) { [weak self] result in
            guard let self = self else { return }
            
            switch result {
            case .success(let response):
                do {
                    let data = try response.map(SourcesResponseEntity.self).sources
                    self.presenter?.interactorDidFetchSourcesList(with: .success(data ?? []))
                } catch {
                    self.presenter?.interactorDidFetchSourcesList(with: .failure(ErrorEnum.failed))
                }
            case .failure:
                self.presenter?.interactorDidFetchSourcesList(with: .failure(ErrorEnum.failed))
            }
        }
    }
}
