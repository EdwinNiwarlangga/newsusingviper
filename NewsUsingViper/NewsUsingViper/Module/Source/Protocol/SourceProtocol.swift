//
//  SourceProtocol.swift
//  NewsUsingViper
//
//  Created by Edwin Niwarlangga on 08/02/24.
//
import Foundation
import UIKit

protocol SourcesPresenterProtocol: class{
    //View -> Presenter
    var view: SourcesViewProtocol? { get set }
    var interactor: SourcesInputInteractorProtocol? { get set }
    var router: SourcesRouterProtocol? { get set }
    
    var categoryNews: String? { get set }
    
    func viewDidLoad(querySearch: String)
    func goToNewsListWithCategoryAndSource(with category: String, source: String, from view: UIViewController)
    
    func loadMoreSources(isSearch: Bool, querySearch: String)
    func searchSources(querySearch : String)
}

protocol SourcesViewProtocol: class{
    // PRESENTER -> VIEW
    func updateSources(with sources: [SourceEntity], isSearch: Bool)
    func updateWithError(with error : String)
}

protocol SourcesInputInteractorProtocol: class{
    //Presenter -> Interactor
    var presenter : SourcesOutputInteractorProtocol? { get set }
    
    func getSources()
}

protocol SourcesOutputInteractorProtocol: class {
    //Interactor -> Presenter
    func interactorDidFetchSourcesList(with result: Result<[SourceEntity], Error>)
}

protocol SourcesRouterProtocol: class{
    //Presenter -> Wireframe
    func pushToArticleList(with category: String, source: String, from view: UIViewController)
    
    static func createListSourceModule(sourceRef: SourceView, category: String)
}

