//
//  SourceRouter.swift
//  NewsUsingViper
//
//  Created by Edwin Niwarlangga on 08/02/24.
//

import Foundation
import UIKit

class SourceRouter: SourcesRouterProtocol{
    func pushToArticleList(with category: String, source: String, from view: UIViewController) {
        let articleView = view.storyboard?.instantiateViewController(withIdentifier: "ArticleView") as! ArticleView
            
        ArticleRouter.createListNewsModule(articleRef: articleView, category: category, source: source)
        view.navigationController?.pushViewController(articleView, animated: true)
    }
    
    static func createListSourceModule(sourceRef: SourceView, category: String) {
        let presenter: SourcesPresenterProtocol & SourcesOutputInteractorProtocol = SourcePresenter()
         
        sourceRef.categoryNews = category
        sourceRef.presenter = presenter
        sourceRef.presenter?.router = SourceRouter()
        sourceRef.presenter?.view = sourceRef
        sourceRef.presenter?.interactor = SourceInteractor()
        sourceRef.presenter?.interactor?.presenter = presenter
    }
}
