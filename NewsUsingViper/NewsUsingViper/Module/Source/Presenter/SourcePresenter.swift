//
//  SourcePresenter.swift
//  NewsUsingViper
//
//  Created by Edwin Niwarlangga on 08/02/24.
//

import Foundation
import UIKit

class SourcePresenter : SourcesPresenterProtocol{
    weak var view: SourcesViewProtocol?
    var interactor: SourcesInputInteractorProtocol?
    var router: SourcesRouterProtocol?
    
    var categoryNews: String?
    var page = 1
    var limit = 10
    
    
    var listSource: [SourceEntity] = []
    
    
    func viewDidLoad(querySearch: String) {
        self.getSourcesArticle(querySearch: querySearch)
    }
    
    func getSourcesArticle(querySearch: String){
        interactor?.getSources()
    }
    func goToNewsListWithCategoryAndSource(with category: String, source: String, from view: UIViewController) {
        router?.pushToArticleList(with: category, source: source, from: view)
    }
    
    func loadMoreSources(isSearch: Bool, querySearch: String) {
        let fromIndex = page * limit
        page += 1
        let toIndex = page * limit
        var newListSource: [SourceEntity] = []
        var isUpdate: Bool?
        
        if querySearch == "" {
            isUpdate = true
            newListSource = Array(listSource[fromIndex..<toIndex])
            view?.updateSources(with: newListSource, isSearch: false)
        } else {
            let filteredSource: [SourceEntity] = self.listSource.filter { SourceEntity in
                SourceEntity.name?.lowercased().contains(querySearch) ?? false
            }
            
            if fromIndex < filteredSource.count{
                isUpdate = true
                if toIndex < filteredSource.count{
                    newListSource = Array(filteredSource[fromIndex..<toIndex])
                } else {
                    newListSource = Array(filteredSource[fromIndex..<filteredSource.count])
                }
            }
            
            view?.updateSources(with: newListSource, isSearch: page == 1)
        }
        
    }
    
    func searchSources(querySearch : String){
        page = 1
        let sizeArray = page * limit
        var newListSource: [SourceEntity] = []
        
        if querySearch != "" {
            let filteredSource: [SourceEntity] = self.listSource.filter { SourceEntity in
                SourceEntity.name?.lowercased().contains(querySearch) ?? false
            }
            newListSource = Array(filteredSource.prefix(sizeArray))
            
            
        } else {
            newListSource = Array(listSource.prefix(sizeArray))
        }
        
        view?.updateSources(with: newListSource, isSearch: true)
        
    }
    
    
}

extension SourcePresenter: SourcesOutputInteractorProtocol{
    func interactorDidFetchSourcesList(with result: Result<[SourceEntity], Error>) {
        switch result{
        case .failure(let error):
            view?.updateWithError(with: error.localizedDescription)
        case .success(let listSources):
            self.listSource = listSources
            let sizeArray = page * limit
            let newListSource = Array(listSource.prefix(sizeArray))
            
            view?.updateSources(with: newListSource, isSearch:  page == 1)
        }
    }
    
}
