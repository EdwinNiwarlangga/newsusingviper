//
//  CategoryEntity.swift
//  NewsUsingViper
//
//  Created by Edwin Niwarlangga on 08/02/24.
//

import Foundation


struct CategoryEntity : Codable {
    let categoryName : String?
    let categoryImage : String?
}
