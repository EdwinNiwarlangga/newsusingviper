//
//  CategoryCollectionViewCell.swift
//  NewsUsingViper
//
//  Created by Edwin Niwarlangga on 08/02/24.
//

import UIKit

class CategoryCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var viewOuter: UIView!
    @IBOutlet weak var imageCell: UIImageView!
    @IBOutlet weak var labelName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        viewOuter.layer.cornerRadius = 20
    }
    
    func setUI(image: String, title: String){
        imageCell.image = UIImage.init(systemName: image)?.withRenderingMode(.alwaysTemplate)
        imageCell.tintColor = UIColor(red: 0.047, green: 0.172, blue: 0.352, alpha: 1.00)
        labelName.text = title.capitalized
    }
}
