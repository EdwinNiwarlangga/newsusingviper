//
//  CategoryView.swift
//  NewsUsingViper
//
//  Created by Edwin Niwarlangga on 08/02/24.
//

import Foundation
import UIKit

class CategoryView: UIViewController, CategoryViewProtocol{
    
    @IBOutlet weak var categoryCollectionView: UICollectionView!
    @IBOutlet weak var categoryTableView: UITableView!
    var presenter: CategoryPresenterProtocol?
    var listCategory = [CategoryEntity]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        CategoryRouter.createCategoryListModule(categoryListRef: self)
        presenter?.viewDidLoad()
        categoryCollectionView.register(UINib.init(nibName: "CategoryCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "categoryCell")
        categoryCollectionView.dataSource = self
        categoryCollectionView.delegate = self
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    func showCategories(with categories: [CategoryEntity]) {
        listCategory = categories
        categoryCollectionView.reloadData()
    }
    
}

extension CategoryView: UICollectionViewDelegate{
    
}

extension CategoryView: UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return listCategory.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "categoryCell", for: indexPath) as! CategoryCollectionViewCell
        let categoryAtItem = listCategory[indexPath.row]
        
        if let imageString = categoryAtItem.categoryImage , let name = categoryAtItem.categoryName{
            cell.setUI(image: imageString, title: name)
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let row = indexPath.row
        let categoryNews = listCategory[row].categoryName ?? "all"
        
        presenter?.goToListNews(category: categoryNews, from: self)
    }
}

extension CategoryView: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        var widthCell : CGSize = CGSize(width: 100, height: 100)
        
        if collectionView == self.categoryCollectionView{
            let layout = collectionView.collectionViewLayout as! UICollectionViewFlowLayout
            layout.sectionInset = UIEdgeInsets(top: 0, left: 8, bottom: 0, right: 8)
            layout.minimumInteritemSpacing = 0
            layout.minimumLineSpacing = 24
            
            widthCell =  CGSize(width: (self.categoryCollectionView.frame.width - 32) / 3 , height: 125) // Set your item size here
        } else {
            widthCell =  CGSize(width: 125 , height:150)
        }
        
        return widthCell
    }
}
