//
//  CategoryRouter.swift
//  NewsUsingViper
//
//  Created by Edwin Niwarlangga on 08/02/24.
//

import Foundation
import UIKit

class CategoryRouter: CategoryRouterProtocol{
    class func createCategoryListModule(categoryListRef: CategoryView) {
       let presenter: CategoryPresenterProtocol & CategoryOutputInteractorProtocol = CategoryPresenter()
        
        categoryListRef.presenter = presenter
        categoryListRef.presenter?.router = CategoryRouter()
        categoryListRef.presenter?.view = categoryListRef
        categoryListRef.presenter?.interactor = CategoryInteractor()
        categoryListRef.presenter?.interactor?.presenter = presenter
    }
    
    func pushToListSource(with category: String, from view: UIViewController) {
        let sourceView = view.storyboard?.instantiateViewController(withIdentifier: "SourceView") as! SourceView
            
        SourceRouter.createListSourceModule(sourceRef: sourceView, category: category)
        view.navigationController?.pushViewController(sourceView, animated: true)
    }
}
