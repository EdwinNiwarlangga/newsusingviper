//
//  ArticleProtocol.swift
//  NewsUsingViper
//
//  Created by Edwin Niwarlangga on 08/02/24.
//

import Foundation
import UIKit

protocol ArticlesPresenterProtocol: class{
    //View -> Presenter
    var view: ArticlesViewProtocol? { get set }
    var interactor: ArticlesInputInteractorProtocol? { get set }
    var router: ArticlesRouterProtocol? { get set }
    
    var categoryNews: String? { get set }
    
    func viewDidLoad(categoryNews: String, source: String, limit: Int, page: Int, querySearch: String)
    func goToDetailNews(url: String, titleArticle: String, from view: UIViewController)
    
    func loadMoreArticles(isSearch: Bool, querySearch: String)
    func searchArticles(querySearch: String)
}

protocol ArticlesViewProtocol: class{
    // PRESENTER -> VIEW
    func updateNews(with news: [ArticlesEntity], isSearch: Bool)
    func updateWithError(with error : String)
}

protocol ArticlesInputInteractorProtocol: class{
    //Presenter -> Interactor
    var presenter : ArticlesOutputInteractorProtocol? { get set }
    
    func getNews(category: String, sourceFrom: String, limit: Int, page: Int, querySearch: String)
}

protocol ArticlesOutputInteractorProtocol: class {
    //Interactor -> Presenter
    func interactorDidFetchNewsListWithCategory(with result: Result<[ArticlesEntity], Error>)
}

protocol ArticlesRouterProtocol: class{
    //Presenter -> Wireframe
    func pushToDetailNews(with url: String, titleArticle: String, from view: UIViewController)
    
    static func createListNewsModule(articleRef: ArticleView, category: String, source: String)
}

