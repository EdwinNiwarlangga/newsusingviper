//
//  ArticleInteractor.swift
//  NewsUsingViper
//
//  Created by Edwin Niwarlangga on 08/02/24.
//

import Foundation
import Moya

class ArticleInteractor : ArticlesInputInteractorProtocol{
    let provider = MoyaProvider<NewsTarget>()
    var presenter: ArticlesOutputInteractorProtocol?
    
    func getNews(category: String, sourceFrom: String, limit: Int, page: Int, querySearch: String) {
        provider.request(.getArticles(page: page, limit: limit, querySearch: querySearch, source: sourceFrom, category: category)) { [weak self] result in
            guard let self = self else { return }
            
            switch result {
            case .success(let response):
                do {
                    let data = try response.map(ArticleResponseEntity.self).articles
                    self.presenter?.interactorDidFetchNewsListWithCategory(with: .success(data ?? []))
                } catch {
                    self.presenter?.interactorDidFetchNewsListWithCategory(with: .failure(ErrorEnum.failed))
                }
            case .failure:
                self.presenter?.interactorDidFetchNewsListWithCategory(with: .failure(ErrorEnum.failed))
            }
        }
    }
}
