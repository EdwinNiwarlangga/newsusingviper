//
//  ArticleRouter.swift
//  NewsUsingViper
//
//  Created by Edwin Niwarlangga on 08/02/24.
//

import Foundation
import UIKit

class ArticleRouter: ArticlesRouterProtocol{
    func pushToDetailNews(with url: String, titleArticle: String, from view: UIViewController) {
        let webViewDetail = view.storyboard?.instantiateViewController(withIdentifier: "DetailArticleView") as! DetailArticleView
        DetailArticleRouter.createDetailArticleModule(detailArticleRef: webViewDetail, urlSourceLink: url, title: titleArticle)
        view.navigationController?.pushViewController(webViewDetail, animated: true)
    }
    
    class func createListNewsModule(articleRef: ArticleView, category: String, source: String) {
       let presenter: ArticlesPresenterProtocol & ArticlesOutputInteractorProtocol = ArticlePresenter()
        
        articleRef.categoryNews = category
        articleRef.sourceFrom = source
        
        articleRef.presenter = presenter
        articleRef.presenter?.router = ArticleRouter()
        articleRef.presenter?.view = articleRef
        articleRef.presenter?.interactor = ArticleInteractor()
        articleRef.presenter?.interactor?.presenter = presenter
    }
    
    deinit {
    }
}
