//
//  ArticleView.swift
//  NewsUsingViper
//
//  Created by Edwin Niwarlangga on 08/02/24.
//

import Foundation
import UIKit

class ArticleView: UIViewController, ArticlesViewProtocol{
   
    var presenter: ArticlesPresenterProtocol?
    var categoryNews: String?
    var sourceFrom: String?
    var countryCode: String?
    var articleList = [ArticlesEntity]()
    var isLoading: Bool = false
    
    @IBOutlet weak var articleTextField: UITextField!
    @IBOutlet weak var articleTableView: UITableView!
    
    @IBOutlet weak var errorView: UIView!
    @IBOutlet weak var errorImageView: UIImageView!
    @IBOutlet weak var errorLabel: UILabel!
    
    
    override func viewDidLoad() {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            LoadingScreen.sharedInstance.showIndicator()
            self.isLoading = true
        }
        errorImageView.image = UIImage(named: "errorImage")
        errorImageView.isHidden = true
        errorLabel.isHidden = true
        errorView.isHidden = true
        
        presenter?.viewDidLoad(categoryNews : categoryNews ?? "all", source: sourceFrom ?? "", limit: 15, page: 1, querySearch: articleTextField.text ?? "")
        
        articleTableView.register(UINib.init(nibName: "ArticleTableViewCell", bundle: .main), forCellReuseIdentifier: "articleCell")
        articleTableView.delegate = self
        articleTableView.dataSource = self
        
        articleTextField.delegate = self
    }
    
    func updateNews(with news: [ArticlesEntity], isSearch: Bool) {
        if isSearch {
            articleList = []
        }
        
        if news.count > 0 {
            news.forEach{ article in
                articleList.append(article)
            }
        } else {
            articleList = news
        }
        
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            LoadingScreen.sharedInstance.hideIndicator()
            self.isLoading = false
            self.articleTableView.reloadData()
            self.articleTableView.isHidden = false
            
        }
    }
    
    func updateWithError(with error: String) {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            self.errorLabel.text = error
            LoadingScreen.sharedInstance.hideIndicator()
            self.articleTableView.isHidden = true
            self.errorLabel.isHidden = false
            self.errorImageView.isHidden = false
            self.errorView.isHidden = false
        }
    }
}


extension ArticleView: UITableViewDelegate{}

extension ArticleView: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return articleList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "articleCell", for: indexPath) as? ArticleTableViewCell
        let row = indexPath.row
        let article = articleList[row]
        
        cell?.setCell(title: article.title ?? "", createdAt: article.publishedAt ?? "", author: article.author ?? "", description: article.url ?? "")
        return cell ?? UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 175
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let row = indexPath.row
        let article = articleList[row]
        
        presenter?.goToDetailNews(url: article.url ?? "", titleArticle: article.title ?? "", from: self)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let getCountArticle = articleList.count
        
        if indexPath.row == getCountArticle - 2 && !isLoading && getCountArticle >= 10{
            isLoading = true
            LoadingScreen.sharedInstance.showIndicator()
            guard let searchText = articleTextField.text else {return}
            presenter?.loadMoreArticles(isSearch: searchText == "" ? false : true, querySearch: searchText)
            
        }
        
    }
    
    
}


extension ArticleView : UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        self.isLoading = true
        LoadingScreen.sharedInstance.showIndicator()
        
        guard let searchText = textField.text else { return false}
        presenter?.searchArticles(querySearch: searchText)
        
        return true
    }
    
    func hideKeyboardWhenTappedAround() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(ArticleView.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}
